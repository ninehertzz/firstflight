export const image = {
    arrow_green: require('../images/arrow.png'),
    uncheck:require('../images/uncheck.png'),
    checked:require('../images/checked.png'),
    SettingIcon:require('../images/SettingIcon.png'),
    editIcon:require('../images/editIcon.png'),
    userIcon:require('../images/userIcon.png'),
    Login: {
        loginbackground: require('../images/login-bg.png'),
        userIocn: require('../images/user.png'),
        lockIcon: require('../images/lock.png'),
        logo: require('../images/logo.png'),
    },
    Signup: {
        signupbackground: require('../images/register-bg.png'),
        back: require('../images/back.png'),
        arrow: require('../images/arrow-down.png'),

    },

    Contact: {
        menu: require('../images/menu.png'),
        more: require('../images/more.png'),
        search: require('../images/search.png'),
        user2: require('../images/user2.png'),
    },

    Split: {
        backleft: require('../images/backleft.png'),
        chart2: require('../images/chart2.png'),

    },

    CheckOut: {
        backleft: require('../images/backleft.png')
    },

    UserProfile: {
        user: require('../images/user.png'),
        Notifications: require('../images/Notifications.png'),
        useremployee: require('../images/useremployee.png'),
        setting: require('../images/setting.png'),
        lock: require('../images/lock.png'),
        Blacklist: require('../images/Blacklist.png'),
        Help: require('../images/Help.png'),
    },

    OwnerProfile: {
        menu: require('../images/menu.png'),
        user: require('../images/user.png'),
    },

    EmployeeProfile: {
        menu: require('../images/menu.png'),
        user: require('../images/user.png'),
    },
    ForgotPassword: {
        signupbackground: require('../images/register-bg.png'),
        back: require('../images/back.png'),
    },
    VerificationCode:{
        signupbackground: require('../images/register-bg.png'),
        back: require('../images/back.png'),
        checkedblack: require('../images/checkedblack.png'),
        uncheckedback: require('../images/uncheckedback.png'),

    },
    Settings:{
        Settingsqr: require('../images/settingsqr.png'),
        Settings: require('../images/settingsuser.png'),
        Settingssymbol: require('../images/settingssymbol.png'),
        Settingsuser1: require('../images/settingsuser1.png'),
        Settingsmessage: require('../images/settingsmessages.png'),
        Settingsbell: require('../images/settingsbell.png'),
        Settingsprice: require('../images/settingsprice.png'),
        Settingsdelete: require('../images/settingsdelete.png'),
        Settingssettingsadd: require('../images/settingsadd.png'),
        // Settingschart: require('../images/settingschart.png'),
        Settingscamera: require('../images/settingscamera.png'),
        Settingsfacebook: require('../images/facebook.png'),
        Settingsinstagram: require('../images/instagram.png'),
        Settingstwitter: require('../images/twitter.png'),

    },
    Inbox:{
        Inboxmenu: require('../images/inboxmenu.png'),
        Inboxadd: require('../images/inboxadd.png'),
        Inboxsearch: require('../images/inboxsearch.png'),
    },
    Group:{
        Groupmenu: require('../images/chatmenu.png'),
        Groupsearch: require('../images/chatsearch.png'),
        Groupuser: require('../images/chatuser.png'),
        Groupadd: require('../images/chatadd.png'),
    },
    Cards:{
        cardmenu: require('../images/menu.png'),
        cardadd: require('../images/chatadd.png'),
        cardvisa: require('../images/visa.png'),
        cardamaxon: require('../images/amaxon.png'),
        cardpaypal: require('../images/paypal.png')
    },
    Payment:{
        Paymentback: require('../images/backleft.png'),
        paymentvisa: require('../images/visa.png'),
    },
    Messages:{
        information: require('../images/information.png'),
        informationuser: require('../images/settingsuser.png'),
        informationcross: require('../images/cross.png'),
        informationloc: require('../images/location1.png'),
        informationvoic: require('../images/voice1.png'),
        informationcamera: require('../images/camera1.png'),

    },
    Chart:{
        chartarrow: require('../images/arrow.png'),
        chartgraph: require('../images/chart.png'),
    },
    MenuScreen:{
        Menuarrow: require('../images/arrow-down.png'),
        Menuburn: require('../images/icon.png'),
    },
    OtpScreen:{
        background: require('../images/login-bgg.png'),
    }

}


