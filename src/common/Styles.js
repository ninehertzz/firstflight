
import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';


export const color = {
  "dgreen": "#66C565",
  "lgreen": "#80D977",
  "vlgreen": "#EEFFEC",
  "gray": "#707070",
  "dgray": "#3B3B3B",
}


export const TxtW = props => <Text {...props} style={[{ fontFamily: 'Arial', color: 'white', fontSize: 16, fontWeight: 'bold' }, props.style]}>{props.children}</Text>
export const TxtB = props => <Text {...props} style={[{ fontFamily: 'Arial', color: '#707070', fontSize: 16, fontWeight: 'bold' }, props.style]}>{props.children}</Text>
export const TxtG = props => <Text {...props} style={[{ fontFamily: 'Arial', color: '#66C565', fontSize: 16, fontWeight: 'bold' }, props.style]}>{props.children}</Text>


export const Roe1by3 = (props) => {
  return (
    <View style={{ flex: 1, flexDirection: 'row', backgroundColor: props.bgColor }}>
      <View style={{ flex: props.children1flexSize, flexDirection: props.direction, alignItems: 'center', }}>
        {props.children1 && <>{props.children}</>}
      </View>

      <View style={{ flex: props.children2flexSize, flexDirection: props.direction, alignItems: 'center' }}>
        {props.children2 && <>{props.children}</>}
      </View>

      <View style={{ flex: props.children3flexSize, flexDirection: props.direction }}>
        {props.children3 && <>{props.children}</>}
      </View>
    </View>
  )
}

export const RowSpaceBetween = (props) => {
  return (
    <View style={styles.container}>
      {props.children}
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between', padding: 10,
    marginVertical: 5, flexDirection: 'row',
    alignItems: 'center', marginVertical: 5,
  }



})