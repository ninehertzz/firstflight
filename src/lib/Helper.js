/*
Install below npm before use it 

npm install --save react-native-loading-spinner-overlay@latest

add this line to App.js in constructor
 Helper.registerLoader(this);
 in render function
 <View style={{ flex: 1 }}>
        <Spinner visible={this.state.loader} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
 </View>
      
npm install react-native-simple-toast --save
 

*/
import * as React from 'react';
import Config from "./Config";
import { NavigationActions, StackActions } from 'react-navigation';
//import Toast from 'react-native-simple-toast';
import Toast from 'react-native-root-toast';
import { AsyncStorage, Alert } from 'react-native';
import Moment from "moment";

import { fetch as fetchPolyfill } from 'whatwg-fetch'
// import { Facebook, } from 'expo';

export default class Helper extends React.Component {

    constructor() {
console.log(Hleper,"HELPPEEEEERRRRRRRRR")
        
    }

        
    

    url = "";
    static paymentdata='';
    static radio_state='';

    static mainApp;
    static appoCount = 0;
    static messageCount = 0;
    static requestCount = 0;
    static toast;
    static user = {};
    static navigationRef;
    static nav = {
        push: (routeName, params) => {
            Helper.navigationRef.dispatch(
                NavigationActions.navigate({
                    routeName, params
                })
            );
            //  self.props.navigation.push (page)
        },
        setRoot: (page, params) => {
            const resetAction = StackActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({ routeName: page, params })
                ]
            })
            Helper.navigationRef.dispatch(resetAction)
        },

        setNavigate: (page, params) => {
            Helper.navigationRef.navigate(page);
        }
    };
 
    static registerNavigator(ref) {
        Helper.navigationRef = ref;

    }
    
    static registerLoader(mainApp) {
        Helper.mainApp = mainApp;
    }


    static getFlotingValue(myStr) {
        return myStr.toFixed(2);
        //return myStr.substring(0, myStr.indexOf(".")+2);

    }
    static showLoader() {
        Helper.mainApp.setState({ loader: true })
    }
    static getImageUrl(url) {
        return finalUrl = Config.Imageurl + url;
    }


    static hideLoader() {
        setTimeout(() => {
            Helper.mainApp.setState({ loader: false })
        }, 1000);

    }


    static getTimeDiffrence(date) {
        let tempdate = new Date();
        let gettime = Moment(tempdate).format("YYYY-MM-DD")
        var ReturnArr = {};
        var days = Moment(date + ' GMT+0300').from(gettime + 'T01:00:05');
        //return days;
        var daysAr = days.split(' ');
        //return days;
        if (days == 'in a day') {
            ReturnArr = { 'type': 2, 'number': 1, 'comment': 'Today' };
            return ReturnArr;
        }

        if (days == 'in a hour') {
            ReturnArr = { 'type': 1, 'number': 1, 'comment': 'In Progress' };
            return ReturnArr;
        }
        else {

            if ((daysAr[2] == 'hour' || daysAr[2] == 'hours') && (daysAr[3] != 'ago')) {
                ReturnArr = { 'type': 2, 'number': 1, 'comment': 'Today' };
                return ReturnArr;

            }
            if ((daysAr[2] == 'day' || daysAr[2] == 'days') && (daysAr[3] != 'ago')) {

                var weeks = daysAr[1] / 7;
                //alert(weeks);
                if (parseInt(weeks) > 0) {
                    //return parseInt(weeks)+' weeks';
                    if (parseInt(weeks) == 1) {
                        ReturnArr = { 'type': 4, 'number': parseInt(weeks), 'comment': parseInt(weeks) + ' Week Away' };
                    } else {
                        ReturnArr = { 'type': 4, 'number': parseInt(weeks), 'comment': parseInt(weeks) + ' Weeks Away' };
                    }

                    return ReturnArr;

                } else {
                    if (daysAr[1] == 1) {
                        ReturnArr = { 'type': 3, 'number': daysAr[1], 'comment': parseInt(daysAr[1]) + ' Day Away' };
                    }
                    else {
                        ReturnArr = { 'type': 3, 'number': daysAr[1], 'comment': parseInt(daysAr[1]) + ' Days Away' };
                    }

                    return ReturnArr;
                }
            }
        }
    }


    static appliedCoupon(baseammount, discountpersentage, type) {
        if (type == "GiftCard") {
            const totalammount = discountpersentage
            return totalammount.toFixed(2)
        }
        if (discountpersentage == undefined) {
            return 0;
        }
        else {
            const totalammount = baseammount * discountpersentage / 100
            return totalammount.toFixed(2)
        }
    }

    static lastUsed(item) {
        console.log(item, "itemitemitemitemitemitemitem****8")
        let diffamt = item.data.coupon.total_amount - (Number(item.data.coupon.stripe_processing_fee) + Number(item.data.coupon.gift_card_commission))

        if (diffamt == (item.data.coupon.discount - item.data.is_used_discount)) {
            return '0.00'
        } else {
            return item.data.is_used_discount
        }
    }

    static TimeDiff(appointmentDate) {
        console.log(appointmentDate, "appointmentDate")
        var now = moment(new Date()).format("DD/MM/YYYY HH:mm:ss")
        var then = moment(new Date()).format("DD/MM/YYYY HH:mm:ss") // "12/11/2019 11:40:00";
        var days = moment.utc(moment(now, "DD/MM/YYYY HH:mm:ss").diff(moment(then, "DD/MM/YYYY HH:mm:ss"))).format("HH:mm:ss")
        console.log(days, "days")
    }

    static amountStripcommision(amount, stripe_fixed, stripe_percent, type) {
        console.log(amount, stripe_fixed, stripe_percent, type, "amount, stripe_fixed, stripe_percent,type")
        if (type == "GiftCard") {


        } else {
            var totalamount = (Number(amount) + Number(stripe_percent)) / (1 - Number(stripe_fixed) / 100)
            var foo = (totalamount - amount)
            return (Number(foo) + 2.5).toFixed(2)
        }



    }


    // static Stripcommision_AfterGc(amount, stripe_fixed, stripe_percent) {
    //     var totalamount = (Number(amount) + Number(stripe_percent)) / (1 - Number(stripe_fixed) / 100)
    //     var foo = (totalamount - amount)
    //     return (Number(foo)+ 2.5).toFixed(2)

    // }

    static amountStripcommision_GiftCard(amount, discount, stripe_percent, stripe_fixed, type) {
        console.log(amount, discount, stripe_percent, stripe_fixed, "amount, stripe_percent, stripe_fixed, discount")
        if (type == "GiftCard" && (Number(amount) < Number(discount))) {
            return 0.0
        } if (type == "GiftCard" && (Number(amount) > Number(discount))) {
            let diff = amount - discount
            let stripOndiff = ((Number(diff) + Number(stripe_fixed)) / (1 - Number(stripe_percent) / 100) - diff)
            const posNum = (stripOndiff < 0) ? stripOndiff * -1 : stripOndiff;
            return posNum.toFixed(2)


        } else {
            var totalamount = (Number(amount - discount) + Number(stripe_fixed)) / (1 - Number(stripe_percent) / 100)
            var foo = (totalamount - Number(amount - discount))
            return foo.toFixed(2)
        }
    }

    static totalamount(amount, stripe_fixed, stripe_percent, type, discountpersentage, amt) {
        if (type == "GiftCard") {
            // let diff = amount - discountpersentage
            const stripComminssion = ((Number(amount) + Number(stripe_percent)) / (1 - Number(stripe_fixed) / 100) - amount)
            const netammount = (amt - discountpersentage)
            let net_Ammount = this.amountStripcommision(netammount, stripe_fixed, stripe_percent)
            if ((Number(amt)  < Number(discountpersentage)))
                return 0
            else {
                const stripOndiff = (Number(netammount) + stripComminssion)
                const posNum = (stripOndiff < 0) ? stripOndiff * -1 : stripOndiff;
                return posNum.toFixed(2)

            }
        }

        const grandtotal = (Number(amount) + Number(stripe_percent)) / (1 - Number(stripe_fixed) / 100)
        return Number(grandtotal + 2.5).toFixed(2)

    }


    static ReleseTotal(item, tip) {
        if (item.transaction.stripe_processing_fee == 0) {
            return tip
        } else {
            let total = item.transaction.coupon && item.transaction.coupon ? Number((item.transaction.total_amount) - (item.transaction.coupon.discount)) + Number(tip) + Number(item.transaction.stripe_processing_fee) : Number(item.transaction.total_amount) + Number(tip) + Number(item.transaction.stripe_processing_fee)
            return total.toFixed(2)

        }

    }
    static PaymnetSymmaryTotal(item) {
        // is coupon 
        let total = item && item.discount ? Number((item.quote_amount) - (item.discount)) + Number(item.tip) + Number(item.stripe_processing_fee) : Number(item.quote_amount) + Number(item.tip) + Number(item.stripe_processing_fee)
        const posNum = (total < 0) ? total * -1 : total;
        return posNum.toFixed(2)


    }




    static DateAndTime(dateTime) {
        dateFormate = Moment.utc(dateTime).local().format('YYYY-MM-DD HH:mm:ss')

        return Moment(dateFormate).format('dddd, MMM Do, hh:mm a');
    }



    static POPDateAndTime(dateTime) {
        dateFormate = Moment.utc(dateTime).local().format('YYYY-MM-DD HH:mm:ss')

        return Moment(dateFormate).format('MMMM DD, YYYY, hh:mm a');
    }

    static OnlyTime(Time) {
        dateFormate = Moment.utc(Time).local().format('YYYY-MM-DD HH:mm:ss')
        return Moment(dateFormate).format('hh:mm a');
    }

    static _getonlyDateFormate(onlyDateFormate) {
        dateFormate = Moment(onlyDateFormate).local().format('YYYY-MM-DD HH:mm:ss')
        //    console.log(onlyDateFormate,"onlyDateFormate" ,"27/09/2019")
        return Moment(dateFormate).format('dddd, MMM Do,YYYY');
    }

    static getonlyDateFormate(onlyDateFormate) {
        dateFormate = Moment.utc(onlyDateFormate).local().format('YYYY-MM-DD HH:mm:ss')
        //    console.log(onlyDateFormate,"onlyDateFormate" ,"27/09/2019")
        return Moment(dateFormate).format('dddd, MMM Do,YYYY');
    }


    static getDateNewFormate(dateFormate) {
        // console.log(dateFormate, "dateFormate")
        dateFormate = Moment.utc(dateFormate).local().format('YYYY-MM-DD HH:mm:ss')
        // if(Helper.tototalCount === true){
        //     alert(dateFormate + 'UTC');

        //     Helper.tototalCount = false

        // }

        return Moment(dateFormate).format('ddd, MMM Do @ hh:mm a');



        // dateFormate = Moment(dateFormate).local().format('YYYY-MM-DD HH:mm:ss')

        // //var new24Time = this.ConvertTimeformat(time);
        // //dateFormate = dateFormate+' '+new24Time+':00';
        // //dateFormate = Moment.utc(dateFormate).local()
        // var ordinal;
        // var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

        // date = new Date(dateFormate.replace(' ', 'T'));

        // const monthNames = ["January", "February", "March", "April", "May", "June",
        //   "July", "August", "September", "October", "November", "December"
        // ];


        // if(date.getUTCDate()>3 && date.getUTCDate()<21) ordinal = 'th'; // thanks kennebec
        //     switch (date.getUTCDate() % 10) {
        //           case 1:  ordinal = "st";
        //           case 2:  ordinal = "nd";
        //           case 3:  ordinal = "rd";
        //           default: ordinal = "th";
        // }


        // var hours = date.getHours();
        // var minutes = date.getMinutes();
        // var ampm = hours >= 12 ? 'pm' : 'am';
        // hours = hours % 12;
        // hours = hours ? hours : 12; // the hour '0' should be '12'
        // minutes = minutes < 10 ? '0'+minutes : minutes;
        // var strTime = hours + ':' + minutes + ' ' + ampm;



        // return days[date.getDay()]+', '+monthNames[date.getMonth()]+' ' +date.getUTCDate() +ordinal +' @ '+ strTime;


    }

    static registerToast(toast) {
        Helper.toast = toast;
    }

    static showToast(msg) {
        if (msg) {
            //Toast.show(msg);
            Toast.show(msg, {
                duration: Toast.durations.SHORT,
                position: Toast.positions.CENTER,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
                onShow: () => {
                    // calls on toast\`s appear animation start
                },
                onShown: () => {
                    // calls on toast\`s appear animation end.
                },
                onHide: () => {
                    // calls on toast\`s hide animation start.
                },
                onHidden: () => {
                    // calls on toast\`s hide animation end.
                }
            });
        }


    }
    static alert(alertMessage, cb) {
        Alert.alert(
            Config.app_name,
            alertMessage,
            [
                { text: 'OK', onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
            ],
            { cancelable: false }
        )
    }



    // static fbAuth(cb) {

    //     this.showLoader();
    //     try {
    //         const {
    //             type,
    //             token,
    //             expires,
    //             permissions,
    //             declinedPermissions,
    //         } = Facebook.logInWithReadPermissionsAsync(Config.fbappid, {
    //             permissions: ['public_profile', 'email']
    //         }).then((response) => {
    //             if (response.type === 'success') {
    //                 fetch('https://graph.facebook.com/me?fields=id,email,name,picture.width(720).height(720).as(picture),friends&access_token=' + response.token)
    //                     .then((response) => response.json())
    //                     .then((json) => {
    //                         console.log("socialData", json);
    //                         this.hideLoader();



    //                         /* 
    //                                                     fetch('https://graph.facebook.com/'+response.token+'/picture?type=large')
    //                                                         .then((responseimage) => responseimage.json())
    //                                                         .then((jsonimage) => {
    //                                                             console.log(jsonimage);

    //                                                         }) */

    //                         /* fetch('https://graph.facebook.com/v3.2/id/permissions?access_token=' + response.token)
    //                             .then((response) => response.json())
    //                             .then((json) => {

    //                             }) */

    //                         let form = {
    //                             social_id: json.id,
    //                             email: '',
    //                             social_type: 'facebook',
    //                             device_token: 'ANDROID',
    //                             device_type: 'ANDROID',
    //                             name: json.name,
    //                             image: ''
    //                         }

    //                         if (json.picture.data.url) {
    //                             form.image = json.picture.data.url;
    //                         }

    //                         if (json.email) {
    //                             form.email = json.email;
    //                             cb(form);
    //                         } else {
    //                             cb(form);
    //                         }

    //                     })
    //                     .catch(() => {
    //                         this.hideLoader();
    //                         console.log('ERROR GETTING DATA FROM FACEBOOK')
    //                     })
    //             } else {
    //                 this.hideLoader();
    //             }
    //         }).catch((err) => {
    //             this.hideLoader();
    //             console.log(JSON.stringify(err), 'ERROR GETTING DATA FROM FACEBOOK')
    //         })


    //     } catch ({ message }) {
    //         this.hideLoader();
    //     }
    // }

    static confirm(alertMessage, cb) {
        Alert.alert(
            Config.app_name,
            alertMessage,
            [
                { text: 'OK', onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
                { text: 'Cancel', onPress: () => { if (cb) cb(false); }, style: 'cancel' },
            ],
            { cancelable: false }
        )
    }

    static permissionConfirm(alertMessage, cb) {
        Alert.alert(
            Config.app_name,
            alertMessage,
            [
                { text: 'NOT NOW', onPress: () => { if (cb) cb(false); }, style: 'cancel' },
                { text: 'SETTINGS', onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
            ],
            { cancelable: false }
        )
    }

    static async  setData(key, val) {
        try {
            let tempval = JSON.stringify(val);
            await AsyncStorage.setItem(key, tempval);
        } catch (error) {
            console.error(error, "AsyncStorage")
            // Error setting data 
        }
    }



    static async  removeData(key, val) {
        try {
            let tempval = JSON.stringify(val);
            await AsyncStorage.removeItem(key, tempval);
        } catch (error) {
            console.error(error, "AsyncStorage")
            // Error setting data 
        }
    }



    static async  getData(key) {
        let value = ""
        try {
            value = await AsyncStorage.getItem(key);
            if (value) {
                let newvalue = JSON.parse(value);
                return newvalue;
            } else {
                return value;
            }

        } catch (error) {

            console.error(error, "AsyncStorage")
            // Error retrieving data
        }
    }

    static validate(form, validations) {
        let isValidForm = true;
        let errors = {};

        let message = '';
        if (!validations) {
            validations = form.validators;
        }
        console.log(form, validations, "validations")
        var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;; //email
        var numberRegex = /^\d+$/; // number
        for (let val in validations) {
            if (!isValidForm) break;
            if (form[val]) {
                for (let i in validations[val]) {
                    var valData = validations[val][i];

                    console.log(form[val], "form[val]")
                    let tempval = form[val];


                    if (i == "required" && !form[val].trim()) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        message = value + " is required";

                    }
                    else if ((i == "minLength" || i == "minLengthDigit") && form[val].length < valData) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        var cStr = i == "minLengthDigit" ? " digit" : " chars";
                        message = value + " should be greater than or equal to " + valData + cStr;

                    }
                    else if ((i == "maxLength" || i == "maxLengthDigit") && form[val].length > valData) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        var cStr = i == "maxLengthDigit" ? " digit" : " chars";
                        message = value + " should be smaller than " + valData + cStr;
                    }
                    else if (i == "matchWith" && form[val] != form[valData]) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        let value2 = (valData.charAt(0).toUpperCase() + valData.slice(1)).split("_").join(" ");
                        message = value + " and " + value2 + " should be same";
                    }
                    else if (i == "email" && reg.test(form[val]) == false) {
                        isValidForm = false;
                        message = "Please enter valid email address";
                    }

                    else if (i == "numeric" && numberRegex.test(form[val]) == false) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        message = value + " should be number only";
                    }

                    if (message) {
                        this.showToast(message);
                        break;
                    }

                }
            }
            else {
                // Helper.showToast("Please enter "+val);
                //form.info[val]="This field is required";
                let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                this.showToast(value + " is required");
                isValidForm = false;
                break;
            }
        }
        return isValidForm;
    }

    static convertHoursMin(totalMinutes){
        var hours = Math.floor(totalMinutes / 60);          
        var minutes = totalMinutes % 60;
        if(hours > 0){
            if(hours == 1){
                return hours+" hour "+Math.trunc(minutes)+' mins'
            }else{
                return hours+" hours "+Math.trunc(minutes)+' mins'
            }
            
        }else{
            return Math.trunc(minutes)+' mins'
        }
        
    }



    static async makeRequest({ url, data, method = "POST", loader = true }) {
        let finalUrl = Config.url + url;
        console.log(finalUrl, "finalUrl");

        let form;
        let methodnew;
        let token = await this.getData("token");
        console.log(token, "tokentoken")
        let varheaders;


        if (method == "POSTUPLOAD") {
            methodnew = "POST";
            /* if (data !== "" && data !== undefined && data !== null) {
                for (var property in data) {
                        form.append(property, data[property]);
                }
            } */

            varheaders = {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data;',
                Authorization: 'Bearer ' + token
            }
            form = data;

            /* form = new FormData();
            for (let i in data)
                form.append(i, data[i]); */ // you can append anyone.

        }
        else if (method == "POST") {
            methodnew = "POST";
            if (token) {
                varheaders = {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization": 'Bearer ' + token
                }
            } else {
                varheaders = {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            }
            form = JSON.stringify(data);
        }
        else {
            methodnew = "GET";
            if (token) {
                varheaders = {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization": 'Bearer ' + token
                }
            } else {
                varheaders = {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                }
            }
        }
        if (loader) this.showLoader();
        console.log("Api chali", form, "form");
        return fetch(finalUrl, {
            body: form,
            method: methodnew,
            headers: varheaders,
        })
            .then((response) => {
                //console.log(response, "response")
                return response.json()
            })
            .then((responseJson) => {
                console.log(responseJson, "responseJson")
                // setTimeout(() => {
                //     if (loader) this.hideLoader();
                // }, 1000);
                if (loader) this.hideLoader();
                if (responseJson.hasOwnProperty('userStatus')) {
                    // AsyncStorage.removeItem('userdata');
                    this.setData('token','')
                    this.setData('userdata','')
                    Helper.navigationRef.navigate('Auth');
                    this.showToast(responseJson.message);
                }


                if (responseJson.loginstatus == false) {
                    this.setData('token','')
                    this.setData('userdata','')
                    Helper.navigationRef.navigate('Auth');
                    this.alert(responseJson.message);
                }
                else
                    return responseJson;

            })
            .catch((error, a) => {
                if (loader) this.hideLoader();
                this.showToast("Please check your internet connection.");
                console.log(error);
            });
    }
}