import { combineReducers } from 'redux';
import {getData,getVehicleData ,getVehicleMake,getVehicleType , getVehicleColor} from './userReducer'

export default combineReducers({
 getData: getData,
 getVehicleData:getVehicleData,
 getVehicleMake:getVehicleMake,
 getVehicleType:getVehicleType,
 getVehicleColor:getVehicleColor

})