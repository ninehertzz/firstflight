import {
    //   EAMIL_CHANGED, PASSWORD_CHANGED,
    FATCHING_DATA,SUCCESS_DATA,FATCHING_ERROR,
    FATCHING_VEHICLE_DATA,SUCCESS_VEHICLE_DATA,VEHICLE_DATA_FAIL,
    FATCHING_VMAKE_DATA,SUCCESS_VMAKE_DATA,FAIL_VMAKE_DATA,
    FATCHING_VTYPE_DATA,SUCCESS_VTYPE_DATA,FAIL_VTYPE_DATA,
    FATCHING_VCOLOR_DATA,SUCCESS_VCOLOR_DATA,FAIL_VCOLOR_DATA

   
} from '../_action/type';

const INITIAL_STATE = {
    data: [],
    error: '',
    loading: false,
    // token:''
}

export const getData = (state = INITIAL_STATE, action) => {
    //  console.log(action);
    switch (action.type) {
        case FATCHING_DATA:
            return {...state, loading: true, error: '' };

        case SUCCESS_DATA:
            return { ...state, data: action.payload, loading: false };

        case FATCHING_ERROR:
            return { error: 'NetWork Error',loading: false }
        default:
            return state;
    }
}


export const getVehicleData = (state = INITIAL_STATE, action) => {
    //  console.log(action);
    switch (action.type) {
        case FATCHING_VEHICLE_DATA:
            return {...state, loading: true, error: '' };

        case SUCCESS_VEHICLE_DATA:
            return { ...state, data: action.payload, loading: false };

        case VEHICLE_DATA_FAIL:
            return { error: 'NetWork Error', loading: false }
        default:
            return state;
    }
}


export const getVehicleMake = (state = INITIAL_STATE, action) => {
    //  console.log(action);
    switch (action.type) {
        case FATCHING_VMAKE_DATA:
            return {...state, loading: true, error: '' };

        case SUCCESS_VMAKE_DATA:
            return { ...state, data: action.payload, loading: false };

        case FAIL_VMAKE_DATA:
            return { error: 'NetWork Error', loading: false }
        default:
            return state;
    }
}


export const getVehicleType = (state = INITIAL_STATE, action) => {
    //  console.log(action);
    switch (action.type) {
        case FATCHING_VTYPE_DATA:
            return {...state, loading: true, error: '' };

        case SUCCESS_VTYPE_DATA:
            return { ...state, data: action.payload, loading: false };

        case FAIL_VTYPE_DATA:
            return { error: 'NetWork Error', loading: false }
        default:
            return state;
    }
}

export const getVehicleColor = (state = INITIAL_STATE, action) => {
    //  console.log(action);
    switch (action.type) {
        case FATCHING_VCOLOR_DATA:
            return {...state, loading: true, error: '' };

        case SUCCESS_VCOLOR_DATA:
            return { ...state, data: action.payload, loading: false };

        case FAIL_VCOLOR_DATA:
            return { error: 'NetWork Error',  loading: false }
        default:
            return state;
    }
}

